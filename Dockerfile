FROM python:3.10
ADD . /login
WORKDIR /login
RUN pip install -r requirements.txt
