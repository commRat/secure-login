#!/bin/sh

# REGISTRATION TESTING - PROPER
curl -X POST -H "Content-Type: application/json" http://localhost:5039/register -d '{"username": "test101", "email": "test1@ahoj.com", "first_name": "John", "last_name": "Jo", "password": "aabcdefgabcdefg", "password_confirm": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/register -d '{"username": "test102", "email": "test2@ahoj.com", "first_name": "Jon", "last_name": "Jox", "password": "aabcdefgabcdefg", "password_confirm": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/register -d '{"username": "test103", "email": "test3@ahoj.com", "first_name": "Johny", "last_name": "Joe", "password": "aabcdefgabcdefg", "password_confirm": "aabcdefgabcdefg"}'

# INVALID REGISTRATIONS
curl -X POST -H "Content-Type: application/json" http://localhost:5039/register -d '{"username": "", "email": "test1@ahoj.com", "first_name": "John", "last_name": "Jo", "password": "aabcdefgabcdefg", "password_confirm": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/register -d '{"username": "test105", "email": "test5ahojcom", "first_name": "John", "last_name": "Jo", "password": "aabcdefgabcdefg", "password_confirm": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/register -d '{"username": "test106", "email": "test6@ahoj.com", "first_name": "John", "last_name": "Jo", "password": "abcd", "password_confirm": "AaAAaAAAAA"}'


# LOGIN TESTING - PROPER
curl -X POST -H "Content-Type: application/json" http://localhost:5039/login -d '{"username": "test101", "password": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/login -d '{"username": "test102", "password": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/login -d '{"username": "test103", "password": "aabcdefgabcdefg"}'

# INVALID LOGIN ATTEMPTS
curl -X POST -H "Content-Type: application/json" http://localhost:5039/login -d '{"username": "test101", "password": "wrongpassword"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/login -d '{"username": "test999090", "password": "aabcdefgabcdefg"}'
curl -X POST -H "Content-Type: application/json" http://localhost:5039/login -d '{"username": "", "password": "aabcdefgabcdefg"}'