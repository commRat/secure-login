## Secure Login

Registration and login. I tryed to keep It as raw as possible, so I am not using flask-login, flask-bcrypt or Jinja templating. Instead of It I use just:

- pure bcrypt for hashing passwords
- flask for handling HTTP requests
- mithril JS for responsive UI
- mongoDB for storing user data
- python, javascript, html, css

Should serve as template for other projects.

<div align="center"><img src="./secure-login/static/demo.png" /></div>

## How to run Secure Login

1) Clone this repo
2) CD into project directory
3) Run command: docker-compose up

After that, app will be running on: http://localhost:5039/

### Troubleshooting

Docker and docker-compose should be installed.

If you run into issue: port is already in use. Just type this command:

    netstat -plntu

There should be running something on same port as Secure Login. For example, if mongoDB is installed locally, It is using 27017 (same as mongoDB from Secure Login). Stop this service by command:

    sudo systemctl stop mongod
