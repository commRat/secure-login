var pwd = null

const Home = {
	oninit: function(vnode) {
		vnode.state.data = vnode.state.data ? vnode.state.data : {}
		vnode.state.message = vnode.state.message ? vnode.state.message : {}
		vnode.state.message.data = vnode.state.message.data ? vnode.state.message.data : {}
		pwd = null;
	},
	view: function(vnode) {
		const userData = vnode.state.data
		const messageData = vnode.state.message
		const loginData = vnode.state.message.data
		JSON.stringify(loginData) === "{}" || loginData == null ? null : pwd = userData.password
		pwd ? m.route.set("/user/" + userData.username) : null
		return m("", {class: "login"}, [
			m("input", {value: null, type: "text", placeholder: "Username", onchange: function(e) {userData.username = e.target.value}}),
			m("input", {value: null, type: "password", placeholder: "Password", onchange: function(e) {userData.password = e.target.value}}),
			m("button", {onclick: function(e) {
				m.request({url: "/login", method: "POST", body: userData}).then(messageData => {vnode.state.message = messageData})
			},
			}, "Login"),
			m("button", {onclick: function(e) {
				m.route.set("/register")
			}}, "Register"),
			messageData ? m("p", messageData.message) : null,
		]);
	}
}

const Register = {
	oninit: function(vnode) {
		const data = {}
		const message = {}
		vnode.state.data = data;
		vnode.state.message = message;
	},
	view: function(vnode) {
		const userData = vnode.state.data
		const messageData = vnode.state.message
		return m("", {class: "login"}, [
			m("input", {value: null, type: "text", placeholder: "Username", onchange: function(e) {userData.username = e.target.value}}),
			m("input", {value: null, type: "text", placeholder: "Email", onchange: function(e) {userData.email = e.target.value}}),
			m("input", {value: null, type: "text", placeholder: "First Name", onchange: function(e) {userData.first_name = e.target.value}}),
			m("input", {value: null, type: "text", placeholder: "Last Name", onchange: function(e) {userData.last_name = e.target.value}}),
			m("input", {value: null, type: "password", placeholder: "Password", onchange: function(e) {userData.password = e.target.value}}),
			m("input", {value: null, type: "password", placeholder: "Password Confirm", onchange: function(e) {userData.password_confirm = e.target.value}}),
			m("button", {onclick: function() {
				m.request({url: "/register", method: "POST", body: userData}).then(message => {vnode.state.message = message})}
			}
			, "Register"),
			messageData ? m("p", messageData.message) : null,
			m("p", "Already have account?"),
			m("button", {onclick: function(e) {
				m.route.set("/")
			}}, "Login"),
		]);
	}
}

const User = {
	oninit: function(vnode) {
		vnode.state.message = {"data": {"username": null, "email": null, "first_name": null, "first_name": null}}
		m.request({url: "/login", method: "POST", body: {"username": vnode.attrs.username, "password": pwd}}).then(logData => {vnode.state.message = logData})
	},
	view: function(vnode) {
		const logData = vnode.state.message.data
		return m("",
		logData.email ?
		[
			m("h1", logData.username),
			m("p.center", "You successfully logged into system!"),
			m("p.center", "Email: ", logData.email),
			m("p.center", "First Name: ", logData.first_name),
			m("p.center", "Last Name: ", logData.last_name),
			m("button.logout", {onclick: function(e) {
				m.route.set("/")
			}}, "Logout")
		] : m("p.white", "Loading ..."))
	}
}

m.route(document.body, "/", {
	"/": Home,
	"/register": Register,
	"/user/:username": User,
})