import flask
from flask import request, jsonify
import logging
import wsgiref.simple_server
import socket
import socketserver
import os
import sys
import secure_login


col = None
basedir = os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__)
app = flask.Flask(__name__, template_folder=os.path.join(basedir, 'templates'), static_folder=os.path.join(basedir, 'static'))
app.config["DEBUG"] = True
srv = None


class MyServer(socketserver.ThreadingMixIn, wsgiref.simple_server.WSGIServer):
    address_family = socket.AF_INET6
    timeout = 1

    def serve_forever(self):
        self._do_serve = 1
        while self._do_serve:
            self.handle_request()


class MyHandler(wsgiref.simple_server.WSGIRequestHandler):
    def handle(self):
        try:
            return super().handle()
        except:
            logging.info('silencing broken pipe error')


@app.route('/login', methods=['GET', 'POST'])
def login():
    user = request.get_json()
    return flask.jsonify(secure_login.login(user, col))


@app.route('/register', methods=['POST'])
def register():
    user = request.get_json()
    return flask.jsonify(secure_login.create_user(user, col))


@app.route("/")
def index():
    return flask.render_template("index.html")


def start(port_):
    global port
    port = port_
    global srv
    logging.basicConfig(level='INFO')
    logging.info("Will listen on port %s" % port_)
    srv = wsgiref.simple_server.make_server('::', port_, app, MyServer, MyHandler)
    srv.serve_forever()


def stop():
    global srv
    if srv:
        srv._do_serve = 0
