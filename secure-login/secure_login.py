import sys
import bcrypt
import logging
from pymongo import MongoClient
import threading
import time
import web
from version import __version__


def mongo_init():
    client = MongoClient('mongodb://db:27017')
    db = client.login_db
    return db.login


def hash(pw):
    return bcrypt.hashpw(pw.encode(), bcrypt.gensalt(rounds=13)).decode()


def check_validity(username, email, first_name, last_name, password, password_confirm, col):
    if username and len(username) > 5 and len(username) < 30:
        user_exists = col.find_one({"username": username})
        if user_exists:
            return {"message": "User already exists."}
    else:
        return {"message": "Username must be greater than 5 characters and less than 30 characters."}

    if email and "@" in email and "." in email:
        email_exists = col.find_one({"email": email})
        if email_exists:
            return {"message": "Email already registered."}
    else:
        return {"message": "Invalid email"}

    if not first_name or not last_name or len(first_name) > 20 or len(last_name) > 20:
        return {"message": "You must enter your name and It has to be smaller than 20 characters."}

    if password and password_confirm:
        if len(password) < 12:
            return {"message": "Password must be greater than 11 characters."}
        if password != password_confirm:
            return {"message": "Password doesn't match."}
    else:
        return {"message": "You must enter password."}
    return None


def create_user(user, col):
    username = user.get("username")
    email = user.get("email")
    first_name = user.get("first_name")
    last_name = user.get("last_name")
    password = user.get("password")
    password_confirm = user.get("password_confirm")

    invalid_details = check_validity(username, email, first_name, last_name, password, password_confirm, col)
    if invalid_details:
        return invalid_details

    col.insert_one({
        "username": username,
        "email": email,
        "first_name": first_name,
        "last_name": last_name,
        "password": hash(password),
    })
    return {"message": "Account created."}


def login(user, col):
    username = user.get("username")
    password = user.get("password")

    if username and password:
        user_data = col.find_one({"username": username})
        if not user_data:
            return {"message": "User does not exists."}
    else:
        return {"message": "You must enter username and password."}

    stored_pw = user_data.get("password")
    del user_data["_id"]
    del user_data["password"]
    return {"data": user_data} if bcrypt.checkpw(password.encode(), stored_pw.encode()) else {"message": "Invalid password."}


def main():
    logging.basicConfig(level='INFO')
    logging.info("Starting Secure Login v%s" % __version__)
    port = 5039
    thr_web = threading.Thread(target=web.start, args=(port, ))
    thr_web.start()
    web.col = mongo_init()
    while 1:
        time.sleep(1)
    web.stop()
    return 0


if __name__ == '__main__':
    sys.exit(main())
